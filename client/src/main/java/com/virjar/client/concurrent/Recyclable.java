package com.virjar.client.concurrent;

/**
 * Description: Recyclable
 *
 * @author lingtong.fu
 * @version 2016-09-04 12:49
 */
public interface Recyclable {
    void recycle();
}
